import os
import threading
import time
import paho.mqtt.client as mqtt
import platform
import xml.etree.ElementTree

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
client = mqtt.Client()


class BatteryMonitor:

    conf_file = ''

    def __init__(self):
        client.on_connect = self.on_connect
        client.on_message = self.on_message
        self.conf_file = cur_dir + 'config.xml'

    def on_connect(self, client, userdata, flags, rc):
        print 'Connected with result code: ', str(rc)
        # TODO: Subscribe all topics of devices...
        for node_name in self.get_param_from_xml('NODE'):
            client.subscribe('{}/temperature'.format(node_name))
            client.subscribe('{}/voltage'.format(node_name))

    def on_message(self, client, userdata, msg):
        """
        Callback function when a new message is subscribed...
        :param client:
        :param userdata:
        :param msg:
        :return:
        """
        topic = msg.topic
        value = msg.payload
        print 'Topic: {}, Value: {}'.format(topic, value)
        data = {
            'Node': topic.split('/')[0],
            topic.split('/')[1]: value
        }
        self.upload_to_aws(data)

    @staticmethod
    def start():
        if platform.system() == 'Windows':
            client.connect('192.168.1.110', 1883, 60)
        else:
            client.connect('127.0.0.1', 1883, 60)

        threading.Thread(target=client.loop_forever).start()

    def upload_to_aws(self, data):
        print data

    def set_param_to_xml(self, tag_name, new_val):
        et = xml.etree.ElementTree.parse(self.conf_file)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file).getroot()
        tmp = []
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp.append(child_of_root.text)
        return tmp

if __name__ == '__main__':
    b = BatteryMonitor()
    b.start()
